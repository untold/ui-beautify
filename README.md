# About UI Beautify

Use ItsTheConnection's UI Beautify improve the quality of your User Interface built using the Unity UI package.

# Installing UI Beautify

To install this package, follow the instructions in the [Package Manager documentation](https://docs.unity3d.com/Packages/com.unity.package-manager-ui@latest/index.html).

# Using UI Beautify

The UI Beautify Manual can be found [here](https://docs.google.com/document/d/1d6iILltewDgMhMWFHvtlxmzLFOZaFzeNpHs4BrjDc9E/edit?usp=sharing).


# Technical details
## Requirements

This version of UI Beautify is compatible with the following versions of the Unity Editor:

* 2022.1 and later (recommended)

## Package contents

The following table indicates the folder structure of the UI Beautify package:

|Location|Description|
|---|---|
|`Editor`|Root folder containing the source for the UI Beautify.|
|`Editor Default Resources`|Root folder containing the graphical Assets for the package.|
|`Runtime`|Root folder containing the source for the runtime of the UI Beautify.|

## Document revision history

|Date|Reason|
|---|---|
|January 24th, 2023|Document created. Matches package version 0.0.0|