# Changelog
All notable changes to this package will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)

---

## [0.0.0] - 2023-01-24
### This is the first setup of UI Beautify, as a Package.