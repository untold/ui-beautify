﻿using UIBeautify.Runtime.Utility;

namespace UIBeautify.Runtime.Interfaces {
    public interface IFeedback {
        public void SetState(SelectionState state, bool instant);
    }
}