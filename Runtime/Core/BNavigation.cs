﻿using System;
using UIBeautify.Runtime.Components;
using UIBeautify.Runtime.Utility;
using UnityEngine;

namespace UIBeautify.Runtime.Core {
    /// <summary>
    /// Structure storing details related to navigation.
    /// </summary>
    [Serializable]
    public struct BNavigation : IEquatable<BNavigation> {
        #region Public Variables
        /// <summary>
        /// Which method of navigation will be used.
        /// </summary>
        [field: SerializeField] public NavigationMode Mode { get; private set; }

        /// <summary>
        /// Enables navigation to wrap around from last to first or first to last element.
        /// Will find the furthest element from the current element in the opposite direction of movement.
        /// </summary>
        /// <example>
        /// Note: If you have a grid of elements and you are on the last element in a row it will not wrap over to the next row it will pick the furthest element in the opposite direction.
        /// </example>
        [field: Tooltip("Enables navigation to wrap around from last to first or first to last element. Does not work for automatic grid navigation")]
        [field: SerializeField] public bool WrapAround { get; private set; }

        /// <summary>
        /// Game object selected when the joystick moves up. Used when navigation is set to "Explicit".
        /// </summary>
        [field: SerializeField] public BSelectable SelectOnUp { get; private set; }
        /// <summary>
        /// Game object selected when the joystick moves down. Used when navigation is set to "Explicit".
        /// </summary>
        [field: SerializeField] public BSelectable SelectOnDown { get; private set; }
        /// <summary>
        /// Game object selected when the joystick moves left. Used when navigation is set to "Explicit".
        /// </summary>
        [field: SerializeField] public BSelectable SelectOnLeft { get; private set; }
        /// <summary>
        /// Game object selected when the joystick moves right. Used when navigation is set to "Explicit".
        /// </summary>
        [field: SerializeField] public BSelectable SelectOnRight { get; private set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Return a BNavigation with sensible default values.
        /// </summary>
        public static BNavigation DefaultNavigation => new() {
            Mode = NavigationMode.Automatic,
            WrapAround = false
        };
        #endregion

        #region Public Methods
        public bool Equals(BNavigation other) => Mode == other.Mode 
            && SelectOnUp == other.SelectOnUp
            && SelectOnDown == other.SelectOnDown
            && SelectOnLeft == other.SelectOnLeft
            && SelectOnRight == other.SelectOnRight;
        #endregion
    }
}