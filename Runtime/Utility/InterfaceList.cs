﻿using System.Collections.Generic;
using UnityEngine;

namespace UIBeautify.Runtime.Utility {
    [System.Serializable]
    public class InterfaceList<T> where T : class {
        #region Public Variables
        [SerializeField] private List<Interface<T>> m_list = new List<Interface<T>>();
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        public IReadOnlyList<Interface<T>> List => m_list;
        #endregion

        #region Constructors
        #endregion

        #region Public Methods
#nullable enable
        public void ForEach(System.Action<T?> action) => m_list.ForEach(i => action?.Invoke(i.Get()));
#nullable restore
        #endregion

        #region Private Methods
        #endregion
    }
}