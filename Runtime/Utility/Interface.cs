﻿using JetBrains.Annotations;
using UnityEngine;

namespace UIBeautify.Runtime.Utility {
    [System.Serializable]
    public class Interface<TInterface> where TInterface : class {
        #region Public Variables
        [SerializeField] private Object m_object;
        #endregion

        #region Public Methods
        [CanBeNull] public TInterface Get() => m_object as TInterface;
        #endregion

        #region Operators
        public static implicit operator bool(Interface<TInterface> i) => i.m_object;
        #endregion
    }
}