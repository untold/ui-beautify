﻿using UIBeautify.Runtime.Components;
using UIBeautify.Runtime.Core;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UIBeautify.Runtime.Utility {
    internal static class BSelUtils {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Public Methods
        internal static void Navigate(this BSelectable sel, AxisEventData eventData) {
            if (sel != null && sel.IsActive())
                eventData.selectedObject = sel.gameObject;
        }

        /// <summary>
        /// Finds the selectable object next to this one.
        /// </summary>
        /// <remarks>
        /// The direction is determined by a Vector3 variable.
        /// </remarks>
        /// <param name="selectable">The selectable to search from.</param>
        /// <param name="dir">The direction in which to search for a neighbouring BSelectable object.</param>
        /// <returns>The neighbouring BSelectable object. Null if none found.</returns>
        public static BSelectable FindSelectable(this BSelectable selectable, Vector3 dir) {
            dir = dir.normalized;
            var localDir = Quaternion.Inverse(selectable.transform.rotation) * dir;
            var pos = selectable.transform.TransformPoint(GetPointOnRectEdge(selectable.transform as RectTransform, localDir));
            var maxScore = Mathf.NegativeInfinity;
            var maxFurthestScore = Mathf.NegativeInfinity;

            var wantsWrapAround = selectable.Navigation.WrapAround && (selectable.Navigation.Mode == NavigationMode.Vertical || selectable.Navigation.Mode == NavigationMode.Horizontal);

            BSelectable bestPick = null;
            BSelectable bestFurthestPick = null;

            for (var i = 0; i < BSelectable.s_SelectableCount; ++i) {
                var sel = BSelectable.s_Selectables[i];

                if (sel == selectable)
                    continue;

                if (!sel.IsInteractable() || sel.Navigation.Mode == NavigationMode.None)
                    continue;

#if UNITY_EDITOR
                // Apart from runtime use, FindSelectable is used by custom editors to
                // draw arrows between different selectables. For scene view cameras,
                // only selectables in the same stage should be considered.
                if (Camera.current != null && !UnityEditor.SceneManagement.StageUtility.IsGameObjectRenderedByCamera(sel.gameObject, Camera.current))
                    continue;
#endif

                var selRect = sel.transform as RectTransform;
                var selCenter = selRect != null ? (Vector3)selRect.rect.center : Vector3.zero;
                var myVector = sel.transform.TransformPoint(selCenter) - pos;

                // Value that is the distance out along the direction.
                var dot = Vector3.Dot(dir, myVector);

                // If element is in wrong direction and we have wrapAround enabled check and cache it if furthest away.
                var score = 0f;
                if (wantsWrapAround && dot < 0) {
                    score = -dot * myVector.sqrMagnitude;

                    if (score > maxFurthestScore) {
                        maxFurthestScore = score;
                        bestFurthestPick = sel;
                    }

                    continue;
                }

                // Skip elements that are in the wrong direction or which have zero distance.
                // This also ensures that the scoring formula below will not have a division by zero error.
                if (dot <= 0)
                    continue;

                // This scoring function has two priorities:
                // - Score higher for positions that are closer.
                // - Score higher for positions that are located in the right direction.
                // This scoring function combines both of these criteria.
                // It can be seen as this:
                //   Dot (dir, myVector.normalized) / myVector.magnitude
                // The first part equals 1 if the direction of myVector is the same as dir, and 0 if it's orthogonal.
                // The second part scores lower the greater the distance is by dividing by the distance.
                // The formula below is equivalent but more optimized.
                //
                // If a given score is chosen, the positions that evaluate to that score will form a circle
                // that touches pos and whose center is located along dir. A way to visualize the resulting functionality is this:
                // From the position pos, blow up a circular balloon so it grows in the direction of dir.
                // The first BSelectable whose center the circular balloon touches is the one that's chosen.
                score = dot / myVector.sqrMagnitude;

                if (score <= maxScore)
                    continue;

                maxScore = score;
                bestPick = sel;
            }

            if (wantsWrapAround && null == bestPick) return bestFurthestPick;

            return bestPick;
        }

        /// <summary>
        /// Find the selectable object to the left of this one.
        /// </summary>
        internal static BSelectable FindSelectableOnLeft(this BSelectable selectable) {
            if (selectable.Navigation.Mode == NavigationMode.Explicit)
                return selectable.Navigation.SelectOnLeft;

            return (selectable.Navigation.Mode & NavigationMode.Horizontal) != 0 ? selectable.FindSelectable(selectable.transform.rotation * Vector3.left) : null;
        }

        /// <summary>
        /// Find the selectable object to the right of this one.
        /// </summary>
        internal static BSelectable FindSelectableOnRight(this BSelectable selectable) {
            if (selectable.Navigation.Mode == NavigationMode.Explicit)
                return selectable.Navigation.SelectOnRight;

            if ((selectable.Navigation.Mode & NavigationMode.Horizontal) != 0)
                return selectable.FindSelectable(selectable.transform.rotation * Vector3.right);

            return null;
        }

        /// <summary>
        /// The BSelectable object above current
        /// </summary>
        internal static BSelectable FindSelectableOnUp(this BSelectable selectable) {
            if (selectable.Navigation.Mode == NavigationMode.Explicit)
                return selectable.Navigation.SelectOnUp;

            if ((selectable.Navigation.Mode & NavigationMode.Vertical) != 0)
                return selectable.FindSelectable(selectable.transform.rotation * Vector3.up);

            return null;
        }

        /// <summary>
        /// Find the selectable object below this one.
        /// </summary>
        internal static BSelectable FindSelectableOnDown(this BSelectable selectable) {
            if (selectable.Navigation.Mode == NavigationMode.Explicit)
                return selectable.Navigation.SelectOnDown;

            if ((selectable.Navigation.Mode & NavigationMode.Vertical) != 0)
                return selectable.FindSelectable(selectable.transform.rotation * Vector3.down);

            return null;
        }
        #endregion

        #region Private Methods
        private static Vector3 GetPointOnRectEdge(RectTransform rect, Vector2 dir) {
            if (rect == null)
                return Vector3.zero;

            if (dir != Vector2.zero)
                dir /= Mathf.Max(Mathf.Abs(dir.x), Mathf.Abs(dir.y));

            var r = rect.rect;
            dir = r.center + Vector2.Scale(r.size, dir * 0.5f);
            return dir;
        }
        #endregion
    }
}