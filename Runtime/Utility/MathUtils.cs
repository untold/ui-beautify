﻿namespace UIBeautify.Runtime.Utility {
    public static class MathUtils {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Public Methods
        public static float Remap (this float value, float oldMin, float oldMax, float newMin, float newMax) => (value - oldMin) / (oldMax - oldMin) * (newMax - newMin) + newMin;
        #endregion

        #region Private Methods
        #endregion
    }
}