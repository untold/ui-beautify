﻿using UnityEngine;
using UnityEngine.UI;

namespace UIBeautify.Runtime.Utility {
    public static class MeshUtils {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Public Methods
        public static void CreateRoundedRectMesh(this VertexHelper vh, Vector4 rect, Rect uvRect, int resolution, float radius, Color32 color) {
            var points = new Vector3[(resolution + 1) * 4 + 4];
            var uvs = new Vector2[(resolution + 1) * 4 + 4];
            var tris = new int[resolution * 4 * 3 + 8 * 3];

            var angleStep = -90f / resolution;
            var cornerIndex = 0;

            // Bottom Left Corner
            var bottomLeftPivot = new Vector3(rect.x + radius, rect.y + radius);

            points[cornerIndex] = bottomLeftPivot;
            uvs[cornerIndex] = new Vector2(points[cornerIndex].x.RelativeX(rect).Remap(0, 1, 0, uvRect.width) + uvRect.x, points[cornerIndex].y.RelativeY(rect).Remap(0, 1, 0, uvRect.height) + uvRect.y);
            vh.AddVert(points[cornerIndex], color, uvs[cornerIndex]);

            for (var i = cornerIndex; i <= cornerIndex + resolution; i++) {
                var vIndex = i + 1;
                points[vIndex] = Quaternion.Euler(0, 0, angleStep * i) * (Vector3.down * radius) + bottomLeftPivot;
                uvs[vIndex] = new Vector2(points[vIndex].x.RelativeX(rect).Remap(0, 1, 0, uvRect.width) + uvRect.x, points[vIndex].y.RelativeY(rect).Remap(0, 1, 0, uvRect.height) + uvRect.y);
                vh.AddVert(points[vIndex], color, uvs[vIndex]);
                var k = i * 3;
                tris[k] = i;
                tris[k + 1] = i + 1;
                tris[k + 2] = cornerIndex;
                vh.AddTriangle(i, i + 1, cornerIndex);
            }

            cornerIndex += resolution + 2;

            // Top Left Corner
            var topLeftPivot = new Vector3(rect.x + radius, rect.w - radius);

            points[cornerIndex] = topLeftPivot;
            uvs[cornerIndex] = new Vector2(points[cornerIndex].x.RelativeX(rect).Remap(0, 1, 0, uvRect.width) + uvRect.x, points[cornerIndex].y.RelativeY(rect).Remap(0, 1, 0, uvRect.height) + uvRect.y);
            vh.AddVert(points[cornerIndex], color, uvs[cornerIndex]);

            for (var i = cornerIndex; i <= cornerIndex + resolution; i++) {
                var vIndex = i + 1;
                points[vIndex] = Quaternion.Euler(0, 0, angleStep * (i - cornerIndex)) * (Vector3.left * radius) + topLeftPivot;
                uvs[vIndex] = new Vector2(points[vIndex].x.RelativeX(rect).Remap(0, 1, 0, uvRect.width) + uvRect.x, points[vIndex].y.RelativeY(rect).Remap(0, 1, 0, uvRect.height) + uvRect.y);
                vh.AddVert(points[vIndex], color, uvs[vIndex]);
                var k = i * 3;
                tris[k] = i;
                tris[k + 1] = i + 1;
                tris[k + 2] = cornerIndex;
                vh.AddTriangle(i, i + 1, cornerIndex);
            }

            cornerIndex += resolution + 2;

            // Top Right Corner
            var topRightPivot = new Vector3(rect.z - radius, rect.w - radius);

            points[cornerIndex] = topRightPivot;
            uvs[cornerIndex] = new Vector2(points[cornerIndex].x.RelativeX(rect).Remap(0, 1, 0, uvRect.width) + uvRect.x, points[cornerIndex].y.RelativeY(rect).Remap(0, 1, 0, uvRect.height) + uvRect.y);
            vh.AddVert(points[cornerIndex], color, uvs[cornerIndex]);

            for (var i = cornerIndex; i <= cornerIndex + resolution; i++) {
                var vIndex = i + 1;
                points[vIndex] = Quaternion.Euler(0, 0, angleStep * (i - cornerIndex)) * (Vector3.up * radius) + topRightPivot;
                uvs[vIndex] = new Vector2(points[vIndex].x.RelativeX(rect).Remap(0, 1, 0, uvRect.width) + uvRect.x, points[vIndex].y.RelativeY(rect).Remap(0, 1, 0, uvRect.height) + uvRect.y);
                vh.AddVert(points[vIndex], color, uvs[vIndex]);
                var k = i * 3;
                tris[k] = i;
                tris[k + 1] = i + 1;
                tris[k + 2] = cornerIndex;
                vh.AddTriangle(i, i + 1, cornerIndex);
            }

            cornerIndex += resolution + 2;

            // Bottom Right Corner
            var bottomRightPivot = new Vector3(rect.z - radius, rect.y + radius);

            points[cornerIndex] = bottomRightPivot;
            uvs[cornerIndex] = new Vector2(points[cornerIndex].x.RelativeX(rect).Remap(0, 1, 0, uvRect.width) + uvRect.x, points[cornerIndex].y.RelativeY(rect).Remap(0, 1, 0, uvRect.height) + uvRect.y);
            vh.AddVert(points[cornerIndex], color, uvs[cornerIndex]);

            for (var i = cornerIndex; i <= cornerIndex + resolution; i++) {
                var vIndex = i + 1;
                points[vIndex] = Quaternion.Euler(0, 0, angleStep * (i - cornerIndex)) * (Vector3.right * radius) + bottomRightPivot;
                uvs[vIndex] = new Vector2(points[vIndex].x.RelativeX(rect).Remap(0, 1, 0, uvRect.width) + uvRect.x, points[vIndex].y.RelativeY(rect).Remap(0, 1, 0, uvRect.height) + uvRect.y);
                vh.AddVert(points[vIndex], color, uvs[vIndex]);
                var k = i * 3;
                tris[k] = i;
                tris[k + 1] = i + 1;
                tris[k + 2] = cornerIndex;
                vh.AddTriangle(i, i + 1, cornerIndex);
            }

            cornerIndex = resolution;
            vh.AddTriangle(0, cornerIndex + 1, cornerIndex + 2);
            vh.AddTriangle(cornerIndex + 2, cornerIndex + 1, cornerIndex + 3);

            cornerIndex += resolution + 2;
            vh.AddTriangle(resolution + 2, cornerIndex + 1, cornerIndex + 2);
            vh.AddTriangle(cornerIndex + 2, cornerIndex + 1, cornerIndex + 3);

            cornerIndex += resolution + 2;
            vh.AddTriangle(resolution * 2 + 4, cornerIndex + 1, cornerIndex + 2);
            vh.AddTriangle(cornerIndex + 2, cornerIndex + 1, cornerIndex + 3);

            vh.AddTriangle(points.Length - resolution - 2, points.Length - 1, 0);
            vh.AddTriangle(0, points.Length - 1, 1);

            vh.AddTriangle(0, resolution + 2, resolution * 2 + 4);
            vh.AddTriangle(0, resolution * 2 + 4, resolution * 3 + 6);
        }

        public static void CreateRoundedBorderMesh(this VertexHelper vh, Vector4 rect, int resolution, float radius, float radiusInner, Color32 color) {
            var angleStep = -90f / resolution;
            var cornerIndex = 0;

            // Bottom Left Corner
            var bottomLeftPivot = new Vector3(rect.x + radius, rect.y + radius);

            for (var i = cornerIndex; i <= cornerIndex + resolution; i++) {
                var angle = Quaternion.Euler(0, 0, angleStep * (i - cornerIndex)) * Vector3.down;

                var point = angle * radius + bottomLeftPivot;
                var uv = new Vector2(point.x.RelativeX(rect), point.y.RelativeY(rect));
                vh.AddVert(point, color, uv);

                var innerPoint = angle * radiusInner + bottomLeftPivot;
                var innerUV = new Vector2(Mathf.InverseLerp(rect.x, rect.z, innerPoint.x), Mathf.InverseLerp(rect.y, rect.w, innerPoint.y));
                vh.AddVert(innerPoint, color, innerUV);
            }

            cornerIndex += resolution + 2;

            // Top Left Corner
            var topLeftPivot = new Vector3(rect.x + radius, rect.w - radius);

            for (var i = cornerIndex; i <= cornerIndex + resolution; i++) {
                var angle = Quaternion.Euler(0, 0, angleStep * (i - cornerIndex)) * Vector3.left;

                var point = angle * radius + topLeftPivot;
                var uv = new Vector2(point.x.RelativeX(rect), point.y.RelativeY(rect));
                vh.AddVert(point, color, uv);

                var innerPoint = angle * radiusInner + topLeftPivot;
                var innerUV = new Vector2(innerPoint.x.RelativeX(rect), innerPoint.y.RelativeY(rect));
                vh.AddVert(innerPoint, color, innerUV);
            }

            cornerIndex += resolution + 2;

            // Top Right Corner
            var topRightPivot = new Vector3(rect.z - radius, rect.w - radius);

            for (var i = cornerIndex; i <= cornerIndex + resolution; i++) {
                var angle = Quaternion.Euler(0, 0, angleStep * (i - cornerIndex)) * Vector3.up;

                var point = angle * radius + topRightPivot;
                var uv = new Vector2(point.x.RelativeX(rect), point.y.RelativeY(rect));
                vh.AddVert(point, color, uv);

                var innerPoint = angle * radiusInner + topRightPivot;
                var innerUV = new Vector2(innerPoint.x.RelativeX(rect), innerPoint.y.RelativeY(rect));
                vh.AddVert(innerPoint, color, innerUV);
            }

            cornerIndex += resolution + 2;

            // Bottom Right Corner
            var bottomRightPivot = new Vector3(rect.z - radius, rect.y + radius);

            for (var i = cornerIndex; i <= cornerIndex + resolution; i++) {
                var angle = Quaternion.Euler(0, 0, angleStep * (i - cornerIndex)) * Vector3.right;

                var point = angle * radius + bottomRightPivot;
                var uv = new Vector2(point.x.RelativeX(rect), point.y.RelativeY(rect));
                vh.AddVert(point, color, uv);

                var innerPoint = angle * radiusInner + bottomRightPivot;
                var innerUV = new Vector2(innerPoint.x.RelativeX(rect), innerPoint.y.RelativeY(rect));
                vh.AddVert(innerPoint, color, innerUV);
            }

            for (var i = 0; i <= (resolution * 2 + 1) * 4; i += 2) {
                vh.AddTriangle(i, i + 2, i + 3);
                vh.AddTriangle(i, i + 3, i + 1);
            }

            var c = vh.currentVertCount - 2;
            vh.AddTriangle(c, 0, 1);
            vh.AddTriangle(c, 1, c + 1);
        }
        #endregion

        #region Private Methods
        private static float RelativeX(this float v, in Vector4 rect) => Mathf.InverseLerp(rect.x, rect.z, v);
        private static float RelativeY(this float v, in Vector4 rect) => Mathf.InverseLerp(rect.y, rect.w, v);
        #endregion
    }
}