﻿namespace UIBeautify.Runtime.Utility {
    /// <summary>
    /// An enumeration of selected states of objects
    /// </summary>
    public enum SelectionState {
        /// <summary>
        /// The UI object can be selected.
        /// </summary>
        Normal,

        /// <summary>
        /// The UI object is highlighted.
        /// </summary>
        Highlighted,

        /// <summary>
        /// The UI object is pressed.
        /// </summary>
        Pressed,

        /// <summary>
        /// The UI object is selected
        /// </summary>
        Selected,

        /// <summary>
        /// The UI object is both selected and highlighted
        /// </summary>
        SelectedAndHighlighted,

        /// <summary>
        /// The UI object cannot be selected.
        /// </summary>
        Disabled,
    }

    /*
     * This looks like it's not flags, but it is flags,
     * the reason is that Automatic is considered horizontal
     * and vertical mode combined
     */
    /// <summary>
    /// BNavigation mode enumeration.
    /// </summary>
    /// <remarks>
    /// This looks like it's not flags, but it is flags, the reason is that Automatic is considered horizontal and vertical mode combined
    /// </remarks>
    [System.Flags]
    public enum NavigationMode {
        /// <summary>
        /// No navigation is allowed from this object.
        /// </summary>
        None = 0,

        /// <summary>
        /// Horizontal BNavigation.
        /// </summary>
        /// <remarks>
        /// BNavigation should only be allowed when left / right move events happen.
        /// </remarks>
        Horizontal = 1,

        /// <summary>
        /// Vertical navigation.
        /// </summary>
        /// <remarks>
        /// BNavigation should only be allowed when up / down move events happen.
        /// </remarks>
        Vertical = 2,

        /// <summary>
        /// Automatic navigation.
        /// </summary>
        /// <remarks>
        /// Attempt to find the 'best' next object to select. This should be based on a sensible heuristic.
        /// </remarks>
        Automatic = 3,

        /// <summary>
        /// Explicit navigation.
        /// </summary>
        /// <remarks>
        /// User should explicitly specify what is selected by each move event.
        /// </remarks>
        Explicit = 4,
    }
}