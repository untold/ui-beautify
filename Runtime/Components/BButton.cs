﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace UIBeautify.Runtime.Components {
    [AddComponentMenu("UI Beautify/Selectables/Button")]
    public class BButton : BSelectable, IPointerClickHandler, ISubmitHandler {
        #region Public Variables
        [field: SerializeField] public UnityEvent OnClick { get; private set; }
        [field: SerializeField] public UnityEvent OnMiddleClick { get; private set; }
        [field: SerializeField] public UnityEvent OnRightClick { get; private set; }
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Overrides
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        private void PressLeft() {
            if (!IsActive() || !IsInteractable())
                return;

            UISystemProfilerApi.AddMarker("BButton.OnClick", this);
            OnClick.Invoke();
        }

        private void PressMiddle() {
            if (!IsActive() || !IsInteractable())
                return;

            UISystemProfilerApi.AddMarker("BButton.OnMiddleClick", this);
            OnMiddleClick.Invoke();
        }

        private void PressRight() {
            if (!IsActive() || !IsInteractable())
                return;

            UISystemProfilerApi.AddMarker("BButton.OnRightClick", this);
            OnRightClick.Invoke();
        }
        #endregion

        #region Interfaces (IPointer...)
        public void OnPointerClick(PointerEventData eventData) {
            switch (eventData.button) {
                case PointerEventData.InputButton.Left:
                    PressLeft();
                    break;
                case PointerEventData.InputButton.Middle:
                    PressMiddle();
                    break;
                case PointerEventData.InputButton.Right:
                    PressRight();
                    break;
                default:
                    return;
            }
        }

        public void OnSubmit(BaseEventData eventData) => PressLeft();
        #endregion
    }
}