﻿using System.Collections.Generic;
using System.Linq;
using UIBeautify.Runtime.Components.Containers;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UIBeautify.Runtime.Components {
    public class BCard : BButton, IBeginDragHandler, IDragHandler, IEndDragHandler {
        #region Public Variables
        #endregion

        #region Private Variables
        private Canvas _canvas;
        private RectTransform _canvasRect;

        private static List<Container> _containers = new List<Container>();

        private Container _previousContainer;
        private int _previousSiblingIndex;

        private bool _isDragging;
        private Vector2 _startDragOffset;
        #endregion

        #region Properties
        #endregion

        #region Behaviour Callbacks
        protected override void Awake() {
            _canvas = GetComponentInParent<Canvas>();
            _canvasRect = _canvas.GetComponent<RectTransform>();
        }
        #endregion

        #region Overrides
        public override void OnPointerDown(PointerEventData eventData) {
            if (_isDragging && eventData.button == PointerEventData.InputButton.Right) {
                CancelDrag();
                return;
            }

            base.OnPointerDown(eventData);
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        private void StartDrag(PointerEventData pointerEventData) {
            if (_isDragging)
                return;

            _previousContainer = GetComponentInParent<Container>();
            _previousSiblingIndex = Rect.GetSiblingIndex();

            _startDragOffset = (Vector2)Rect.position - pointerEventData.position;

            _containers = _canvas.GetComponentsInChildren<Container>().ToList();
            _previousContainer?.RemoveContent(this);

            Rect.SetAsLastSibling();

            _isDragging = true;

            Rect.ForceUpdateRectTransforms();
        }

        private void Dragging(PointerEventData pointerEventData) {
            var mPos = pointerEventData.position;
            Rect.position = mPos + _startDragOffset;

            Rect.ForceUpdateRectTransforms();
        }

        private void CancelDrag() {
            _previousContainer?.AddContent(this);
            Rect.SetSiblingIndex(_previousSiblingIndex);

            _isDragging = false;

            Rect.ForceUpdateRectTransforms();
        }

        private void EndDrag(PointerEventData pointerEventData) {
            if (!_isDragging)
                return;

            var mPos = RectTransformUtility.WorldToScreenPoint(Camera.current, pointerEventData.position);
            var index = _containers.FindIndex(c => c.ContainsPoint(mPos));

            if (index < 0) {
                CancelDrag();
                return;
            }

            _containers[index].AddContent(this);
            _containers.Clear();

            _isDragging = false;

            Rect.ForceUpdateRectTransforms();
        }
        #endregion

        #region Interfaces (IDrag...)
        public void OnBeginDrag(PointerEventData eventData) {
            if (eventData.button != PointerEventData.InputButton.Left)
                return;

            StartDrag(eventData);
        }

        public void OnDrag(PointerEventData eventData) {
            if (!_isDragging)
                return;

            if (!eventData.dragging)
                return;

            Dragging(eventData);
        }

        public void OnEndDrag(PointerEventData eventData) {
            if (eventData.button != PointerEventData.InputButton.Left)
                return;

            EndDrag(eventData);
        }
        #endregion
    }
}