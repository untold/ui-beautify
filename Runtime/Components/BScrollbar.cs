﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace UIBeautify.Runtime.Components {
    [AddComponentMenu("UI Beautify/Selectables/Scrollbar")]
    public class BScrollbar : BSelectable, IBeginDragHandler, IDragHandler, IEndDragHandler {
        protected enum Direction { LeftToRight, RightToLeft, BottomToTop, TopToBottom }

        #region Public Variables
        [SerializeField] private Direction m_direction;
        [SerializeField, Range(0.01f, 1f)] private float m_barWidthDefaultPercent = .2f;

        [SerializeField] private RectTransform m_scrollArea;
        [SerializeField] private RectTransform m_handle;

        [field: SerializeField] public UnityEvent<float> OnValueChanged { get; private set; }
        [field: SerializeField] public UnityEvent<float> OnValueChanging { get; private set; }
        #endregion

        #region Private Variables
        private bool _isDragging;
        private float _preDragValue;
        #endregion

        #region Properties
        public float BarWidthPercent { get; private set; }
        public float Position { get; private set; } = 0f;
        public float Percent { get; private set; } = 0f;
        #endregion

        #region Behaviour Callbacks
        protected override void Awake() {
            BarWidthPercent = m_barWidthDefaultPercent;
            UpdateVisual();
        }
        #endregion

        #region Overrides
        public override void OnPointerDown(PointerEventData eventData) {
            if (_isDragging && eventData.button == PointerEventData.InputButton.Right) {
                CancelDrag();
                return;
            }

            base.OnPointerDown(eventData);
        }
        #endregion

        #region Public Methods
        public void SetValueWithoutNotify(float value) {
            Percent = value;
            Position = Mathf.Lerp(0f, 1f - BarWidthPercent, Percent);
            UpdateVisual();
        }

        public void SetRatio(float ratio) {
            if (ratio is <= 0f or > 1f) {
                Debug.LogWarning("Ratio must be between 0+ and 1\n", this);
                return;
            }

            BarWidthPercent = ratio;
            Position = Mathf.Clamp(Position, 0f, 1f - BarWidthPercent);
            Percent = Position / (1f - BarWidthPercent);

            UpdateVisual();
            OnValueChanged?.Invoke(Percent);
        }
        #endregion

        #region Private Methods
        private void StartDrag() {
            _preDragValue = Position;
            _isDragging = true;
        }

        private void Dragging(PointerEventData pointerEventData) {
            var mPos = m_scrollArea.InverseTransformPoint(pointerEventData.position);
            mPos -= (Vector3)m_handle.rect.size * .5f;
            var relPos = mPos / m_scrollArea.rect.size; // Ranges from -0.5f to 0.5f

            Position = m_direction switch {
                Direction.LeftToRight => .5f + relPos.x,
                Direction.RightToLeft => .5f - relPos.x,
                Direction.BottomToTop => relPos.y,
                Direction.TopToBottom => 1 - relPos.y,
                _ => 0f
            };
            Position = Mathf.Clamp(Position, 0f, 1f - BarWidthPercent);
            Percent = Position / (1f - BarWidthPercent);

            UpdateVisual();
            OnValueChanging?.Invoke(Percent);
        }

        private void CancelDrag() {
            Position = _preDragValue;
            Percent = Position / (1f - BarWidthPercent);
            _isDragging = false;

            UpdateVisual();
            OnValueChanged?.Invoke(Percent);
        }

        private void EndDrag() {
            if (!_isDragging)
                return;

            _isDragging = false;
            OnValueChanged?.Invoke(Percent);
        }

        private void UpdateVisual() {
            if (!m_handle)
                return;

            switch (m_direction) {
                case Direction.LeftToRight:
                    m_handle.anchorMin = new Vector2(Position, .5f);
                    m_handle.anchorMax = new Vector2(Position + BarWidthPercent, .5f);
                    break;
                case Direction.RightToLeft:
                    m_handle.anchorMin = m_handle.anchorMax = new Vector2(1 - Position, .5f);
                    break;
                case Direction.BottomToTop:
                    m_handle.anchorMin = m_handle.anchorMax = new Vector2(.5f, Position);
                    break;
                case Direction.TopToBottom:
                    m_handle.anchorMin = m_handle.anchorMax = new Vector2(.5f, 1 - Position);
                    break;
                default:
                    return;
            }

            m_handle.ForceUpdateRectTransforms();
        }
        #endregion

        #region Interfaces (IDrag...)
        public void OnBeginDrag(PointerEventData eventData) {
            if (eventData.button == PointerEventData.InputButton.Left)
                StartDrag();
        }

        public void OnDrag(PointerEventData eventData) {
            if (!_isDragging)
                return;

            if (!eventData.dragging)
                return;

            Dragging(eventData);
        }

        public void OnEndDrag(PointerEventData eventData) {
            EndDrag();
        }
        #endregion
    }
}