﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UIBeautify.Runtime.Components.Containers {
    [AddComponentMenu("UI Beautify/Containers/Container")]
    public class Container : UIBehaviour {
        #region Public Variables
        #endregion

        #region Private Variables
        private RectTransform _canvasRect;
        private RectTransform _rectTransform;
        #endregion

        #region Properties
        public List<BSelectable> Contents { get; private set; } = new List<BSelectable>();
        #endregion

        #region Behaviour Callbacks
        protected override void Awake() {
            _canvasRect = GetComponentInParent<Canvas>().GetComponent<RectTransform>();
            _rectTransform = GetComponent<RectTransform>();

            foreach (var selectable in GetComponentsInChildren<BSelectable>())
                Contents.Add(selectable);
        }
        #endregion

        #region Public Methods
        public void AddContent(in BSelectable selectable) {
            if (Contents.Contains(selectable)) {
                Debug.LogWarning($"The container already contains {selectable.name}\n", this);
                return;
            }

            Contents.Add(selectable);
            selectable.Rect.SetParent(transform, true);
            selectable.Rect.ForceUpdateRectTransforms();
        }

        public void RemoveContent(in BSelectable selectable) {
            if (!Contents.Contains(selectable)) {
                Debug.LogWarning($"The element {selectable.name} is not a child of the container\n", this);
                return;
            }

            Contents.Remove(selectable);
            selectable.Rect.SetParent(_canvasRect, true);
            selectable.Rect.ForceUpdateRectTransforms();
        }

        public bool ContainsPoint(Vector2 screenPoint) => RectTransformUtility.RectangleContainsScreenPoint(_rectTransform, screenPoint);
        #endregion

        #region Private Methods
        #endregion
    }
}