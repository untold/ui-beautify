﻿using UIBeautify.Runtime.Interfaces;
using UIBeautify.Runtime.Utility;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace UIBeautify.Runtime.Components {
    [AddComponentMenu("UI Beautify/Selectables/Toggle")]
    public class BToggle : BSelectable, IPointerClickHandler, ISubmitHandler {
        #region Public Variables
        [SerializeField, HideInInspector] private bool m_isOn;
        [SerializeField] private Interface<IFeedback> m_indicator;

        [field: SerializeField] public UnityEvent<bool> OnValueChanged { get; private set; }
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        public bool IsOn {
            get => m_isOn;
            set {
                if (value == m_isOn)
                    return;

                m_isOn = value;
                ValueChanged();
            }
        }
        #endregion

        #region Behaviour Callbacks
        protected override void Awake() => ValueChanged();
        #endregion

        #region Overrides
        #endregion

        #region Public Methods
        public void SetValueWithoutNotify(bool value) {
            m_isOn = value;
            m_indicator.Get()?.SetState(m_isOn ? SelectionState.Highlighted : SelectionState.Normal, true);
        }
        #endregion

        #region Private Methods
        private void PressLeft() {
            if (!IsActive() || !IsInteractable())
                return;

            UISystemProfilerApi.AddMarker("BBToggle.OnValueChanged", this);
            IsOn = !m_isOn;
        }

        private void ValueChanged() {
            m_indicator.Get()?.SetState(m_isOn ? SelectionState.Highlighted : SelectionState.Normal, true);
            OnValueChanged?.Invoke(m_isOn);
        }
        #endregion

        #region Interfaces (IPointer...)
        public void OnPointerClick(PointerEventData eventData) {
            switch (eventData.button) {
                case PointerEventData.InputButton.Left:
                    PressLeft();
                    break;
                default:
                    return;
            }
        }

        public void OnSubmit(BaseEventData eventData) => PressLeft();
        #endregion
    }
}