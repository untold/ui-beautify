﻿using UIBeautify.Runtime.Interfaces;
using UIBeautify.Runtime.Utility;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UIBeautify.Runtime.Components {
    internal sealed class FeedbackDispatcher : UIBehaviour, IFeedback {
        #region Public Variables
        [SerializeField] private InterfaceList<IFeedback> m_feedbacks;
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion

        #region Interfaces (IFeedback)
        public void SetState(SelectionState state, bool instant) => m_feedbacks.ForEach(f => f?.SetState(state, instant));
        #endregion
    }
}