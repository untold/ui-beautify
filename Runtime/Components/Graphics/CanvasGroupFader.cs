﻿using UIBeautify.Runtime.Interfaces;
using UIBeautify.Runtime.Utility;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UIBeautify.Runtime.Components.Graphics {
    [ExecuteInEditMode]
    [AddComponentMenu("UI Beautify/Feedbacks/Canvas Group Fader")]
    [RequireComponent(typeof(CanvasGroup))]
    public class CanvasGroupFader : UIBehaviour, IFeedback {
        #region Public Variables
        [SerializeField] private float m_normalAlpha;
        [SerializeField] private float m_activeAlpha;
        #endregion

        #region Private Variables
        private CanvasGroup _group;
        #endregion

        #region Properties
        #endregion

        #region Behaviour Callbacks
#if UNITY_EDITOR
        protected override void OnValidate() {
            _group ??= GetComponent<CanvasGroup>();
        }
#endif

        protected override void Awake() {
            base.Awake();

            _group ??= GetComponent<CanvasGroup>();
            _group.alpha = m_normalAlpha;
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion

        #region Interfaces (IFeedback)
        public void SetState(SelectionState state, bool instant) {
            switch (state) {
                case SelectionState.Disabled:
                case SelectionState.Normal:
                    _group.alpha = m_normalAlpha;
                    break;
                case SelectionState.Highlighted:
                case SelectionState.SelectedAndHighlighted:
                case SelectionState.Pressed:
                case SelectionState.Selected:
                default:
                    _group.alpha = m_activeAlpha;
                    break;
            }
        }
        #endregion
    }
}