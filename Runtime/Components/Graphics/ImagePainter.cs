﻿using UIBeautify.Runtime.Interfaces;
using UIBeautify.Runtime.Utility;
using UnityEngine;
using UnityEngine.UI;

namespace UIBeautify.Runtime.Components.Graphics {
    [AddComponentMenu("UI Beautify/Feedbacks/Image Painter")]
    public class ImagePainter : Image, IFeedback {
        #region Public Variables
        [SerializeField] private Color m_normalColor;
        [SerializeField] private Color m_selectedColor;
        [SerializeField] private Color m_highlightColor;
        [SerializeField] private Color m_selectedHighlightColor;
        [SerializeField] private Color m_pressedColor;
        [SerializeField] private Color m_disabledColor;
        [SerializeField, Min(0.01f)] private float m_duration = .1f;
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Behaviour Callbacks
        protected override void Awake() {
            base.Awake();

            color = m_normalColor;
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion

        #region Interfaces (IFeedback)
        public void SetState(SelectionState state, bool instant) {
            var targetColor = state switch {
                SelectionState.Highlighted => m_highlightColor,
                SelectionState.SelectedAndHighlighted => m_selectedHighlightColor,
                SelectionState.Selected => m_selectedColor,
                SelectionState.Normal => m_normalColor,
                SelectionState.Pressed => m_pressedColor,
                SelectionState.Disabled => m_disabledColor,
                _ => m_normalColor
            };

            if (instant || !isActiveAndEnabled)
                color = targetColor;
            else {
                color = Color.white;
                CrossFadeColor(targetColor, m_duration, true, true);
            }
        }
        #endregion
    }
}