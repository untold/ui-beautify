using UnityEngine;
using UnityEngine.EventSystems;

namespace UIBeautify.Runtime.Components.Graphics {
    [ExecuteInEditMode]
    [AddComponentMenu("UI Beautify/Feedbacks/Rect Anchorer")]
    public class RectTransformAnchorer : UIBehaviour {
        #region Public Variables
        [SerializeField] private RectTransform m_anchorRect;
        [SerializeField] private Vector2 m_anchorPoint;
        [SerializeField] private Vector2 m_anchorOffset;
        #endregion

        #region Private Variables
        private RectTransform _rectTransform;
        #endregion

        #region Properties
        #endregion

        #region Behaviour Callbacks
#if UNITY_EDITOR
        protected override void OnValidate() {
            _rectTransform ??= GetComponent<RectTransform>();
        }
#endif

        protected override void Awake() {
            base.Awake();

            _rectTransform ??= GetComponent<RectTransform>();
            _rectTransform.position = m_anchorRect.TransformPoint((m_anchorPoint - Vector2.right * .5f) * m_anchorRect.rect.size + m_anchorOffset);
        }

        private void Update() {
            _rectTransform.position = m_anchorRect.TransformPoint((m_anchorPoint - Vector2.right * .5f) * m_anchorRect.rect.size + m_anchorOffset);
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}