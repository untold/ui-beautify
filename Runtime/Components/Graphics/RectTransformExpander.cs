﻿using System.Collections;
using UIBeautify.Runtime.Interfaces;
using UIBeautify.Runtime.Utility;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UIBeautify.Runtime.Components.Graphics {
    [AddComponentMenu("UI Beautify/Feedbacks/Rect Expander")]
    public class RectTransformExpander : UIBehaviour, ILayoutSelfController, IFeedback {
        #region Public Variables
        [SerializeField] private Vector2 m_normalSize;
        [SerializeField] private Vector2 m_selectedSize;
        [SerializeField] private Vector2 m_highlightSize;
        [SerializeField] private Vector2 m_selectedHighlightSize;
        [SerializeField] private Vector2 m_pressedSize;
        [SerializeField] private Vector2 m_disabledSize;
        [SerializeField, Min(0.01f)] private float m_duration = .1f;
        #endregion

        #region Private Variables
        private RectTransform _rectTransform;
        private Coroutine _scaleCoroutine;
        #endregion

        #region Properties
        #endregion

        #region Behaviour Callbacks
#if UNITY_EDITOR
        protected override void Reset() {
            m_normalSize = m_selectedSize = m_highlightSize = m_selectedHighlightSize = m_pressedSize = m_disabledSize = _rectTransform.sizeDelta;
        }

        protected override void OnValidate() {
            _rectTransform ??= GetComponent<RectTransform>();
        }
#endif

        protected override void Awake() {
            base.Awake();

            _rectTransform ??= GetComponent<RectTransform>();
            _rectTransform.sizeDelta = m_normalSize;
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        private IEnumerator SizeRectCO(Vector2 targetSize) {
            var t = 0f;
            var startSize = _rectTransform.sizeDelta;

            while (t < m_duration) {
                t += Time.unscaledDeltaTime;
                _rectTransform.sizeDelta = Vector2.Lerp(startSize, targetSize, t / m_duration);
                yield return null;
            }

            _rectTransform.sizeDelta = targetSize;
        }
        #endregion

        #region Interfaces (ILayoutSelfController)
        public void SetLayoutHorizontal() {
            
        }

        public void SetLayoutVertical() {
            
        }
        #endregion

        #region Interfaces (IFeedback)
        public void SetState(SelectionState state, bool instant) {
            var targetSize = state switch {
                SelectionState.Highlighted => m_highlightSize,
                SelectionState.SelectedAndHighlighted => m_selectedHighlightSize,
                SelectionState.Selected => m_selectedSize,
                SelectionState.Normal => m_normalSize,
                SelectionState.Pressed => m_pressedSize,
                SelectionState.Disabled => m_disabledSize,
                _ => m_normalSize
            };

            if (instant || !isActiveAndEnabled)
                _rectTransform.sizeDelta = targetSize;
            else {
                if (_scaleCoroutine != null)
                    StopCoroutine(_scaleCoroutine);
                _scaleCoroutine = StartCoroutine(SizeRectCO(targetSize));
            }
        }
        #endregion
    }
}