﻿using UIBeautify.Runtime.Utility;
using UnityEngine;
using UnityEngine.UI;

namespace UIBeautify.Runtime.Components.Graphics {
    public class ImageRounded : Image {
        #region Public Variables
        [SerializeField] private float m_radius = 10f;
        [SerializeField] private float m_radiusInner = 5f;
        [SerializeField, Min(1)] private int m_resolution = 5;
        [SerializeField] private bool m_fill = true;
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Behaviour Callbacks
        protected override void OnPopulateMesh(VertexHelper vh) {
            var tex = mainTexture;
            vh.Clear();

            if (tex == null)
                return;

            var r = GetPixelAdjustedRect();
            var v = new Vector4(r.x, r.y, r.x + r.width, r.y + r.height);
            if (m_fill)
                vh.CreateRoundedRectMesh(v, new Rect(0, 0, 1, 1), m_resolution, m_radius, color);
            else
                vh.CreateRoundedBorderMesh(v, m_resolution, m_radius, m_radiusInner, color);
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}