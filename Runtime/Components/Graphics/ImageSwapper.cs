﻿using UIBeautify.Runtime.Interfaces;
using UIBeautify.Runtime.Utility;
using UnityEngine;
using UnityEngine.UI;

namespace UIBeautify.Runtime.Components.Graphics {
    [AddComponentMenu("UI Beautify/Feedbacks/Image Swapper")]
    public class ImageSwapper : Image, IFeedback {
        #region Public Variables
        [SerializeField] private Sprite m_defaultSprite;
        [SerializeField] private Sprite m_spriteToSwap;
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Behaviour Callbacks
        protected override void Awake() {
            base.Awake();

            sprite = m_defaultSprite;
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion

        #region Interfaces (IFeedback)
        public void SetState(SelectionState state, bool instant) {
            switch (state) {
                case SelectionState.Highlighted:
                case SelectionState.SelectedAndHighlighted:
                    sprite = m_spriteToSwap;
                    break;
                case SelectionState.Selected:
                case SelectionState.Normal:
                case SelectionState.Pressed:
                case SelectionState.Disabled:
                default:
                    sprite = m_defaultSprite;
                    break;
            }
        }
        #endregion
    }
}