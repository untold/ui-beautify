﻿using System.Collections;
using UIBeautify.Runtime.Interfaces;
using UIBeautify.Runtime.Utility;
using UnityEngine;
using UnityEngine.UI;

namespace UIBeautify.Runtime.Components.Graphics {
    [AddComponentMenu("UI Beautify/Feedbacks/Image Zoomer")]
    public class ImageZoomer : Image, IFeedback {
        #region Public Variables
        [SerializeField, Range(0f, 0.95f)] private float m_normalZoom = 0f;
        [SerializeField, Range(0f, 0.95f)] private float m_hoverZoom = .1f;
        [SerializeField, Min(0.01f)] private float m_time;
        #endregion

        #region Private Variables
        private float _previousZoom, _targetZoom;
        private Coroutine _zoomCoroutine;
        #endregion

        #region Properties
        private float CurrentZoom { get; set; }
        #endregion

        #region Behaviour Callbacks
#if UNITY_EDITOR
        protected override void OnValidate() {
            type = Image.Type.Simple;
            CurrentZoom = m_normalZoom;
            UpdateGeometry();
            UpdateMaterial();
        }
#endif

        protected override void Awake() {
            base.Awake();

            CurrentZoom = m_normalZoom;
        }

        protected override void OnPopulateMesh(VertexHelper vh) {
            var verts = new Vector3[] {
                new(0f, 0f, 0f),
                new(0f, 1f, 0f),
                new(1f, 1f, 0f),
                new(1f, 0f, 0f)
            };
            rectTransform.GetLocalCorners(verts);
            var zoom = CurrentZoom * .5f;

            var uvs = new Vector2[4];
            if (preserveAspect) {
                var aspect = rectTransform.rect.width / rectTransform.rect.height;
                var spriteAspect = sprite.rect.width / sprite.rect.height;

                if (spriteAspect < aspect) {
                    // Source higher than rect
                    var h = (spriteAspect / aspect) * .5f;
                    var relativeAspect = spriteAspect / aspect;
                    var relativeZoom = relativeAspect * zoom;
                    uvs[0] = new Vector2(0f + zoom, .5f - h + relativeZoom);
                    uvs[1] = new Vector2(0f + zoom, .5f + h - relativeZoom);
                    uvs[2] = new Vector2(1f - zoom, .5f + h - relativeZoom);
                    uvs[3] = new Vector2(1f - zoom, .5f - h + relativeZoom);
                } else {
                    // Source lower than rect
                    var w = (aspect / spriteAspect) * .5f;
                    var relativeAspect = aspect / spriteAspect;
                    var relativeZoom = relativeAspect * zoom;
                    uvs[0] = new Vector2(.5f - w + relativeZoom, 0f + zoom);
                    uvs[1] = new Vector2(.5f - w + relativeZoom, 1f - zoom);
                    uvs[2] = new Vector2(.5f + w - relativeZoom, 1f - zoom);
                    uvs[3] = new Vector2(.5f + w - relativeZoom, 0f + zoom);
                }
            } else {
                uvs[0] = new Vector2(0f + zoom, 0f + zoom);
                uvs[1] = new Vector2(0f + zoom, 1f - zoom);
                uvs[2] = new Vector2(1f - zoom, 1f - zoom);
                uvs[3] = new Vector2(1f - zoom, 0f + zoom);
            }

            vh.Clear();

            for (var i = 0; i < verts.Length; i++)
                vh.AddVert(verts[i], color, uvs[i]);

            vh.AddTriangle(0, 1, 2);
            vh.AddTriangle(0, 2, 3);
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        private void ApplyZoom(float targetZoom) {
            CurrentZoom = targetZoom;
            UpdateGeometry();
        }

        private IEnumerator ZoomCO() {
            var t = 0f;
            while (t < m_time) {
                t += Time.unscaledDeltaTime;
                CurrentZoom = Mathf.Lerp(_previousZoom, _targetZoom, t / m_time);
                UpdateGeometry();
                yield return null;
            }

            ApplyZoom(_targetZoom);
        }
        #endregion

        #region Interfaces (IFeedback)
        public void SetState(SelectionState state, bool instant) {
            _previousZoom = CurrentZoom;

            switch (state) {
                case SelectionState.Highlighted:
                case SelectionState.SelectedAndHighlighted:
                    _targetZoom = m_hoverZoom;
                    break;
                case SelectionState.Selected:
                case SelectionState.Normal:
                case SelectionState.Pressed:
                case SelectionState.Disabled:
                default:
                    _targetZoom = m_normalZoom;
                    break;
            }

            if (instant || !isActiveAndEnabled) {
                ApplyZoom(_targetZoom);
                return;
            }

            if (_zoomCoroutine != null)
                StopCoroutine(_zoomCoroutine);
            _zoomCoroutine = StartCoroutine(ZoomCO());
        }
        #endregion
    }
}