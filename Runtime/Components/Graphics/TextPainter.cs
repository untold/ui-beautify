﻿#if TEXT_MESH_PRO
using System.Collections;
using UIBeautify.Runtime.Interfaces;
using UIBeautify.Runtime.Utility;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

namespace UIBeautify.Runtime.Components.Graphics {
    [AddComponentMenu("UI Beautify/Feedbacks/Text Painter")]
    [RequireComponent(typeof(TMP_Text))]
    public class TextPainter : UIBehaviour, IFeedback {
        #region Public Variables
        [SerializeField] private Color m_normalColor;
        [SerializeField] private Color m_selectedColor;
        [SerializeField] private Color m_highlightColor;
        [SerializeField] private Color m_selectedHighlightColor;
        [SerializeField] private Color m_pressedColor;
        [SerializeField] private Color m_disabledColor;
        [SerializeField, Min(0.01f)] private float m_duration = .1f;
        #endregion

        #region Private Variables
        private TMP_Text _text;
        private Coroutine _fadeCoroutine;
        #endregion

        #region Properties
        #endregion

        #region Behaviour Callbacks
        protected override void Awake() {
            base.Awake();

            _text ??= GetComponent<TMP_Text>();
            _text.color = m_normalColor;
        }

        protected override void OnDisable() {
            if (_fadeCoroutine != null)
                StopCoroutine(_fadeCoroutine);
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        private void CrossFadeColor(Color targetColor, float duration, bool ignoreTimeScale, bool instant) {
            if (_fadeCoroutine != null)
                StopCoroutine(_fadeCoroutine);

            if (instant) {
                _text.color = targetColor;
                return;
            }

            _fadeCoroutine = StartCoroutine(CrossFadeColorCO(targetColor, duration, ignoreTimeScale));
        }

        private IEnumerator CrossFadeColorCO(Color targetColor, float duration, bool ignoreTimeScale) {
            var fromColor = _text.color;
            var t = 0f;

            while (t < duration) {
                t += ignoreTimeScale ? Time.unscaledDeltaTime : Time.deltaTime;
                _text.color = Color.Lerp(fromColor, targetColor, t / duration);
                yield return null;
            }

            _text.color = targetColor;
            _fadeCoroutine = null;
        }
        #endregion

        #region Interfaces (IFeedback)
        public void SetState(SelectionState state, bool instant) {
            var targetColor = state switch {
                SelectionState.Highlighted => m_highlightColor,
                SelectionState.SelectedAndHighlighted => m_selectedHighlightColor,
                SelectionState.Selected => m_selectedColor,
                SelectionState.Normal => m_normalColor,
                SelectionState.Pressed => m_pressedColor,
                SelectionState.Disabled => m_disabledColor,
                _ => m_normalColor
            };

            if (instant || !isActiveAndEnabled) {
                _text ??= GetComponent<TMP_Text>();
                _text.color = targetColor;
            } else
                CrossFadeColor(targetColor, m_duration, true, true);
        }
        #endregion
    }
}
#endif