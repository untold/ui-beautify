﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UIBeautify.Runtime.Components {
    [AddComponentMenu("UI Beautify/Selectables/Slider")]
    public class BSlider : BSelectable, IBeginDragHandler, IDragHandler, IEndDragHandler {
        protected enum Direction { LeftToRight, RightToLeft, BottomToTop, TopToBottom }

        #region Public Variables
        [SerializeField] private Direction m_direction;
        [SerializeField] private float m_minValue = 0f;
        [SerializeField] private float m_maxValue = 1f;
        [SerializeField/*, HideInInspector*/] private float m_value;

        [SerializeField] private RectTransform m_scrollArea;
        [SerializeField] private Image m_filler;
        [SerializeField] private RectTransform m_handle;

        [field: SerializeField] public UnityEvent<float> OnValueChanged { get; private set; }
        [field: SerializeField] public UnityEvent<float> OnValueChanging { get; private set; }
        #endregion

        #region Private Variables
        private bool _isDragging;
        private float _preDragValue;
        #endregion

        #region Properties
        public float Percent { get; private set; }
        #endregion

        #region Behaviour Callbacks
        protected override void Awake() {
            Percent = Mathf.InverseLerp(m_minValue, m_maxValue, m_value);
            UpdateVisual();
        }
        #endregion

        #region Overrides
        public override void OnPointerDown(PointerEventData eventData) {
            if (_isDragging && eventData.button == PointerEventData.InputButton.Right) {
                CancelDrag();
                return;
            }

            base.OnPointerDown(eventData);
        }
        #endregion

        #region Public Methods
        public void SetValueWithoutNotify(float value) {
            m_value = value;
            UpdateVisual();
        }
        #endregion

        #region Private Methods
        private void StartDrag() {
            _preDragValue = m_value;
            _isDragging = true;
        }

        private void Dragging(PointerEventData pointerEventData) {
            var mPos = m_scrollArea.InverseTransformPoint(pointerEventData.position);
            var relPos = mPos / m_scrollArea.rect.size; // Ranges from -0.5f to 0.5f

            Percent = m_direction switch {
                Direction.LeftToRight => .5f + relPos.x,
                Direction.RightToLeft => .5f - relPos.x,
                Direction.BottomToTop => relPos.y,
                Direction.TopToBottom => 1 - relPos.y,
                _ => 0f
            };
            Percent = Mathf.Clamp01(Percent);

            m_value = Mathf.Lerp(m_minValue, m_maxValue, Percent);

            UpdateVisual();
            OnValueChanging?.Invoke(m_value);
        }

        private void CancelDrag() {
            m_value = _preDragValue;
            Percent = Mathf.InverseLerp(m_minValue, m_maxValue, m_value);
            _isDragging = false;

            UpdateVisual();
            OnValueChanged?.Invoke(m_value);
        }

        private void EndDrag() {
            if (!_isDragging)
                return;

            _isDragging = false;
            OnValueChanged?.Invoke(m_value);
        }

        private void UpdateVisual() {
            if (!m_handle)
                return;

            switch (m_direction) {
                case Direction.LeftToRight:
                    m_handle.anchorMin = m_handle.anchorMax = new Vector2(Percent, .5f);
                    break;
                case Direction.RightToLeft:
                    m_handle.anchorMin = m_handle.anchorMax = new Vector2(1 - Percent, .5f);
                    break;
                case Direction.BottomToTop:
                    m_handle.anchorMin = m_handle.anchorMax = new Vector2(.5f, Percent);
                    break;
                case Direction.TopToBottom:
                    m_handle.anchorMin = m_handle.anchorMax = new Vector2(.5f, 1 - Percent);
                    break;
                default:
                    return;
            }

            m_filler.fillAmount = Percent;
            m_handle.ForceUpdateRectTransforms();
        }
        #endregion

        #region Interfaces (IDrag...)
        public void OnBeginDrag(PointerEventData eventData) {
            if (eventData.button == PointerEventData.InputButton.Left)
                StartDrag();
        }

        public void OnDrag(PointerEventData eventData) {
            if (!_isDragging)
                return;

            if (!eventData.dragging)
                return;

            Dragging(eventData);
        }

        public void OnEndDrag(PointerEventData eventData) {
            EndDrag();
        }
        #endregion
    }
}