﻿using System;
using System.Collections.Generic;
using UIBeautify.Runtime.Core;
using UIBeautify.Runtime.Interfaces;
using UIBeautify.Runtime.Utility;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UIBeautify.Runtime.Components {
    public delegate void OnStateChangedDelegate(SelectionState state, bool instant);

    /// <summary>
    /// Simple selectable object - derived from to create a selectable control.
    /// </summary>
    [AddComponentMenu("UI/BSelectable", 35)]
    [ExecuteAlways]
    [SelectionBase]
    [DisallowMultipleComponent]
    public class BSelectable : UIBehaviour, IMoveHandler, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler, ISelectHandler, IDeselectHandler {
        #region Public Variables
        [Tooltip("Whether this BSelectable reacts to input events")]
        [SerializeField] private bool m_Interactable = true;
        [SerializeField] private Interface<IFeedback> m_feedback;

        [SerializeField] private BNavigation m_navigation = BNavigation.DefaultNavigation;
        [SerializeField] private bool m_deselectOnMouseExit = true;

        public event OnStateChangedDelegate OnStateChanged;
        #endregion

        #region Private Variables
        internal static BSelectable[] s_Selectables = new BSelectable[10];
        internal static int s_SelectableCount;
        
        private readonly List<CanvasGroup> _canvasGroupCache = new List<CanvasGroup>();

        private bool _enableCalled;

        private bool _groupsAllowInteraction = true;
        protected int _currentIndex = -1;
        #endregion

        #region Properties
        /// <summary>
        /// Copy of the array of all the selectable objects currently active in the scene.
        /// </summary>
        public static BSelectable[] AllSelectablesArray {
            get {
                var temp = new BSelectable[s_SelectableCount];
                Array.Copy(s_Selectables, temp, s_SelectableCount);
                return temp;
            }
        }

        /// <summary>
        /// How many selectable elements are currently active.
        /// </summary>
        public static int AllSelectableCount => s_SelectableCount;

        /// <summary>
        /// The Navigation setting for this selectable object.
        /// </summary>
        public BNavigation Navigation {
            get => m_navigation;
            set {
                if (PropUtils.SetStruct(ref m_navigation, value))
                    OnSetProperty();
            }
        }

        public RectTransform Rect { get; private set; }

        public SelectionState State {
            get {
                if (!IsInteractable())
                    return SelectionState.Disabled;
                if (IsPointerDown)
                    return SelectionState.Pressed;
                if (HasSelection)
                    return IsPointerInside ? SelectionState.SelectedAndHighlighted : SelectionState.Selected;
                if (IsPointerInside)
                    return SelectionState.Highlighted;
                return SelectionState.Normal;
            }
        }

        /// <summary>
        /// Is this object interactable.
        /// </summary>
        public bool Interactable {
            get => m_Interactable;
            set {
                if (!PropUtils.SetStruct(ref m_Interactable, value))
                    return;

                if (!m_Interactable && EventSystem.current != null && EventSystem.current.currentSelectedGameObject == gameObject)
                    EventSystem.current.SetSelectedGameObject(null);

                OnSetProperty();
            }
        }

        public bool IsPointerInside { get; private set; }
        public bool IsPointerDown { get; private set; }
        public bool HasSelection { get; private set; }
        #endregion

        #region Constructors
        protected BSelectable() {}
        #endregion

        #region Behaviour Callback
#if UNITY_EDITOR
        protected override void OnValidate() {
            base.OnValidate();

            // OnValidate can be called before OnEnable, this makes it unsafe to access other components
            // since they might not have been initialized yet.
            // OnSetProperty potentially access Animator or Graphics. (case 618186)
            if (!isActiveAndEnabled)
                return;

            if (!Interactable && EventSystem.current != null && EventSystem.current.currentSelectedGameObject == gameObject)
                EventSystem.current.SetSelectedGameObject(null);

            // And now go to the right state.
            DoStateTransition(State, true);
        }
#endif

        // Select on enable and add to the list.
        protected override void OnEnable() {
            //Check to avoid multiple OnEnable() calls for each selectable
            if (_enableCalled)
                return;

            base.OnEnable();

            Rect ??= GetComponent<RectTransform>();

            if (s_SelectableCount == s_Selectables.Length) {
                var temp = new BSelectable[s_Selectables.Length * 2];
                Array.Copy(s_Selectables, temp, s_Selectables.Length);
                s_Selectables = temp;
            }

            if (EventSystem.current && EventSystem.current.currentSelectedGameObject == gameObject)
                HasSelection = true;

            _currentIndex = s_SelectableCount;
            s_Selectables[_currentIndex] = this;
            s_SelectableCount++;
            IsPointerDown = false;
            DoStateTransition(State, true);

            _enableCalled = true;
        }

        protected override void OnCanvasGroupChanged() {
            // Figure out if parent groups allow interaction
            // If no interaction is allowed... then we need
            // to not do that :)
            var groupAllowInteraction = true;
            var t = transform;
            while (t != null) {
                t.GetComponents(_canvasGroupCache);
                var shouldBreak = false;
                foreach (var cachedGroup in _canvasGroupCache) {
                    // if the parent group does not allow interaction
                    // we need to break
                    if (cachedGroup.enabled && !cachedGroup.interactable) {
                        groupAllowInteraction = false;
                        shouldBreak = true;
                    }
                    // if this is a 'fresh' group, then break
                    // as we should not consider parents
                    if (cachedGroup.ignoreParentGroups)
                        shouldBreak = true;
                }
                if (shouldBreak)
                    break;

                t = t.parent;
            }

            if (groupAllowInteraction == _groupsAllowInteraction)
                return;

            _groupsAllowInteraction = groupAllowInteraction;
            OnSetProperty();
        }

        // Call from unity if animation properties have changed
        protected override void OnDidApplyAnimationProperties() => OnSetProperty();

        protected override void OnTransformParentChanged() {
            base.OnTransformParentChanged();

            // If our parenting changes figure out if we are under a new CanvasGroup.
            OnCanvasGroupChanged();
        }

        // Remove from the list.
        protected override void OnDisable() {
            //Check to avoid multiple OnDisable() calls for each selectable
            if (!_enableCalled)
                return;

            s_SelectableCount--;

            // Update the last elements index to be this index
            s_Selectables[s_SelectableCount]._currentIndex = _currentIndex;

            // Swap the last element and this element
            s_Selectables[_currentIndex] = s_Selectables[s_SelectableCount];

            // null out last element.
            s_Selectables[s_SelectableCount] = null;

            InstantClearState();
            base.OnDisable();

            _enableCalled = false;
        }

        private void OnApplicationFocus(bool hasFocus) {
            if (!hasFocus && IsPressed())
                InstantClearState();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Non allocating version for getting the all selectables.
        /// If selectables.Length is less then s_SelectableCount only selectables.Length elements will be copied which
        /// could result in a incomplete list of elements.
        /// </summary>
        /// <param name="selectables">The array to be filled with current selectable objects</param>
        /// <returns>The number of element copied.</returns>
        public static int AllSelectablesNoAlloc(BSelectable[] selectables) {
            var copyCount = selectables.Length < s_SelectableCount ? selectables.Length : s_SelectableCount;

            Array.Copy(s_Selectables, selectables, copyCount);

            return copyCount;
        }

        /// <summary>
        /// Is the object interactable.
        /// </summary>
        public virtual bool IsInteractable() => _groupsAllowInteraction && m_Interactable;

        /// <summary>
        /// Returns whether the selectable is currently 'highlighted' or not.
        /// </summary>
        /// <remarks>
        /// Use this to check if the selectable UI element is currently highlighted.
        /// </remarks>
        public bool IsHighlighted() => IsActive() && IsInteractable() && (IsPointerInside && !IsPointerDown && !HasSelection);

        /// <summary>
        /// Whether the current selectable is being pressed.
        /// </summary>
        public bool IsPressed() => IsActive() && IsInteractable() && IsPointerDown;

        /// <summary>
        /// Selects this BSelectable.
        /// </summary>
        public virtual void Select() {
            if (EventSystem.current == null || EventSystem.current.alreadySelecting)
                return;

            EventSystem.current.SetSelectedGameObject(gameObject);
        }
        #endregion

        #region Private Methods
        private void OnSetProperty() {
#if UNITY_EDITOR
            // ReSharper disable once ConvertIfStatementToConditionalTernaryExpression
            if (!Application.isPlaying)
                DoStateTransition(State, true);
            else
#endif
                DoStateTransition(State, false);
        }

        /// <summary>
        /// Clear any internal state from the BSelectable (used when disabling).
        /// </summary>
        protected void InstantClearState() {
            IsPointerInside = false;
            IsPointerDown = false;
            HasSelection = false;
        }

        /// <summary>
        /// Transition the BSelectable to the entered state.
        /// </summary>
        /// <param name="state">State to transition to</param>
        /// <param name="instant">Should the transition occur instantly.</param>
        protected void DoStateTransition(SelectionState state, bool instant) {
            OnStateChanged?.Invoke(state, instant || !gameObject.activeInHierarchy);
            m_feedback.Get()?.SetState(state, instant);
        }

        // Change the button to the correct state
        private void EvaluateAndTransitionToSelectionState() {
            if (!IsActive() || !IsInteractable())
                return;

            DoStateTransition(State, false);
        }
        #endregion

        // ----------- Selection logic -----------

        /// <summary>
        /// Determine in which of the 4 move directions the next selectable object should be found.
        /// </summary>
        public virtual void OnMove(AxisEventData eventData) {
            switch (eventData.moveDir) {
                case MoveDirection.Right:
                    this.FindSelectableOnRight()?.Navigate(eventData);
                    break;
                case MoveDirection.Up:
                    this.FindSelectableOnUp()?.Navigate(eventData);
                    break;
                case MoveDirection.Left:
                    this.FindSelectableOnLeft()?.Navigate(eventData);
                    break;
                case MoveDirection.Down:
                    this.FindSelectableOnDown()?.Navigate(eventData);
                    break;
            }
        }

        #region Interfaces (IPointer...)
        /// <summary>
        /// Evaluate current state and transition to pressed state.
        /// </summary>
        public virtual void OnPointerDown(PointerEventData eventData) {
            if (eventData.button != PointerEventData.InputButton.Left)
                return;

            // Selection tracking
            if (IsInteractable() && Navigation.Mode != NavigationMode.None && EventSystem.current != null)
                EventSystem.current.SetSelectedGameObject(gameObject, eventData);

            IsPointerDown = true;
            EvaluateAndTransitionToSelectionState();
        }

        /// <summary>
        /// Evaluate eventData and transition to appropriate state.
        /// </summary>
        public virtual void OnPointerUp(PointerEventData eventData) {
            if (eventData.button != PointerEventData.InputButton.Left)
                return;

            IsPointerDown = false;
            EvaluateAndTransitionToSelectionState();
        }

        /// <summary>
        /// Evaluate current state and transition to appropriate state.
        /// New state could be pressed or hover depending on pressed state.
        /// </summary>
        public virtual void OnPointerEnter(PointerEventData eventData) {
            if (eventData == null || eventData.pointerEnter == null || eventData.pointerEnter.GetComponentInParent<BSelectable>() != this)
                return;

            IsPointerInside = true;
            EvaluateAndTransitionToSelectionState();
        }

        /// <summary>
        /// Evaluate current state and transition to normal state.
        /// </summary>
        public virtual void OnPointerExit(PointerEventData eventData) {
            IsPointerInside = false;

            if (m_deselectOnMouseExit && EventSystem.current.currentSelectedGameObject == gameObject)
                EventSystem.current.SetSelectedGameObject(null);

            EvaluateAndTransitionToSelectionState();
        }

        /// <summary>
        /// Set selection and transition to appropriate state.
        /// </summary>
        public virtual void OnSelect(BaseEventData eventData) {
            HasSelection = true;
            EvaluateAndTransitionToSelectionState();
        }

        /// <summary>
        /// Unset selection and transition to appropriate state.
        /// </summary>
        public virtual void OnDeselect(BaseEventData eventData) {
            HasSelection = false;
            EvaluateAndTransitionToSelectionState();
        }
        #endregion
    }
}