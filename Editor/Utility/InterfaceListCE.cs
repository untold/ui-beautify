﻿using System.Reflection;
using UIBeautify.Runtime.Utility;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace UIBeautify.Editors.Utility {
    [CustomPropertyDrawer(typeof(InterfaceList<>), true)]
    public class InterfaceListCE : PropertyDrawer {
        #region Constants
        private const BindingFlags k_fieldFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;
        #endregion

        #region Public Variables
        #endregion

        #region Private Variables
        private SerializedProperty _listProp;
        private ReorderableList _reorderableList;
        #endregion

        #region Properties
        #endregion

        #region Behaviour Callbacks
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
            _listProp ??= property.FindPropertyRelative("m_list");

            var height = _listProp.arraySize * EditorGUIUtility.singleLineHeight + (_listProp.arraySize - 1) * EditorGUIUtility.standardVerticalSpacing;
            height += EditorGUIUtility.singleLineHeight * 3 + EditorGUIUtility.standardVerticalSpacing * 4;

            return height;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            _listProp ??= property.FindPropertyRelative("m_list");

            _reorderableList ??= new ReorderableList(property.serializedObject, _listProp, true, true, true, true) {
                drawHeaderCallback = DrawHeaderList,
                drawNoneElementCallback = DrawEmptyList,
                elementHeight = EditorGUIUtility.singleLineHeight,
                drawElementCallback = DrawElement,
                onAddCallback = AddElement,
                onRemoveCallback = RemoveElement,
                onCanRemoveCallback = CanRemoveElement
            };

            _reorderableList.DoList(position);
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion

        #region Event Methods
        private void DrawHeaderList(Rect rect) {
            // _listProp.isExpanded = EditorGUI.Foldout(rect, _listProp.isExpanded, _listProp.displayName, true);
            EditorGUI.LabelField(rect, _listProp.displayName, EditorStyles.boldLabel);
        }

        private void DrawEmptyList(Rect rect) {
            EditorGUI.LabelField(rect, "The list is empty", EditorStyles.centeredGreyMiniLabel);
        }

        private void DrawElement(Rect rect, int index, bool isActive, bool isFocused) {
            rect.height -= EditorGUIUtility.standardVerticalSpacing;
            var ithProp = _listProp.GetArrayElementAtIndex(index);
            EditorGUI.PropertyField(rect, ithProp);
        }

        private void AddElement(ReorderableList list) => _listProp.InsertArrayElementAtIndex(_listProp.arraySize);

        private void RemoveElement(ReorderableList list) => _listProp.DeleteArrayElementAtIndex(list.index);

        private bool CanRemoveElement(ReorderableList list) => list.index >= 0 && list.index < _listProp.arraySize;
        #endregion
    }
}