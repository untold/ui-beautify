﻿using System.Reflection;
using UIBeautify.Runtime.Interfaces;
using UIBeautify.Runtime.Utility;
using UnityEditor;
using UnityEngine;

namespace UIBeautify.Editors.Utility {
    [CustomPropertyDrawer(typeof(Interface<>), true)]
    public class InterfaceCE : PropertyDrawer {
        #region Constants
        private const BindingFlags k_fieldFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;
        #endregion

        #region Public Variables
        #endregion

        #region Private Variables
        private System.Type _interfaceType;

        private SerializedProperty _objProp;
        #endregion

        #region Properties
        #endregion

        #region Behaviour Callbacks
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            _objProp = property.FindPropertyRelative("m_object");
            if (property.propertyPath.EndsWith("]"))
                _interfaceType = fieldInfo.FieldType.GenericTypeArguments[0].GenericTypeArguments[0];
            else
                _interfaceType = fieldInfo.FieldType.GenericTypeArguments[0];
            EditorGUI.ObjectField(position, _objProp, _interfaceType, label);

            var e = Event.current;
            if (!position.Contains(e.mousePosition))
                return;

            switch (e.type) {
                case EventType.DragUpdated:
                    if (DragAndDrop.objectReferences[0] is not GameObject go)
                        break;

                    DragAndDrop.visualMode = go.TryGetComponent(out IFeedback _) ? DragAndDropVisualMode.Link : DragAndDropVisualMode.Rejected;

                    e.Use();
                    break;
                case EventType.DragPerform:
                    if (DragAndDrop.objectReferences[0] is not GameObject goj)
                        break;

                    if (!goj.TryGetComponent(out IFeedback feedback))
                        break;

                    _objProp.objectReferenceValue = (Object)feedback;
                    DragAndDrop.AcceptDrag();

                    e.Use();
                    break;
                default:
                    break;
            }
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}