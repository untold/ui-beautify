﻿using UIBeautify.Runtime.Components;
using UnityEditor;

namespace UIBeautify.Editors.Components {
    [CanEditMultipleObjects]
    [CustomEditor(typeof(BSelectable), true)]
    public class BSelectableCE : Editor {
        #region Public Variables
        #endregion

        #region Private Variables
        private SerializedProperty _interactableProp;
        private SerializedProperty _feedbackProp;
        private SerializedProperty _navigationProp;
        private SerializedProperty _deselectOnMouseExitProp;
        #endregion

        #region Properties
        #endregion

        #region Behaviour Callbacks
        protected virtual void OnEnable() {
            _interactableProp = serializedObject.FindProperty("m_Interactable");
            _feedbackProp = serializedObject.FindProperty("m_feedback");
            _navigationProp = serializedObject.FindProperty("m_navigation");
            _deselectOnMouseExitProp = serializedObject.FindProperty("m_deselectOnMouseExit");
        }

        public override void OnInspectorGUI() {
            serializedObject.Update();

            EditorGUILayout.PropertyField(_interactableProp);
            EditorGUILayout.PropertyField(_feedbackProp);
            EditorGUILayout.PropertyField(_navigationProp);
            EditorGUILayout.PropertyField(_deselectOnMouseExitProp);

            EditorGUILayout.Space();

            var prop = serializedObject.FindProperty(_deselectOnMouseExitProp.propertyPath);
            while (prop.NextVisible(false))
                EditorGUILayout.PropertyField(prop);

            serializedObject.ApplyModifiedProperties();
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}