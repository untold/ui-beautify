﻿using System.Reflection;
using UIBeautify.Runtime.Components;
using UnityEditor;

namespace UIBeautify.Editors.Components {
    [CanEditMultipleObjects]
    [CustomEditor(typeof(BToggle))]
    public class BToggleCE : BSelectableCE {
        #region Public Variables
        #endregion

        #region Private Variables
        private SerializedProperty _isOnProp;

        private MethodInfo _setValueWithoutNotifyMI;
        #endregion

        #region Properties
        #endregion

        #region Behaviour Callbacks
        protected override void OnEnable() {
            base.OnEnable();

            _isOnProp = serializedObject.FindProperty("m_isOn");
            _setValueWithoutNotifyMI = serializedObject.targetObject.GetType().GetMethod("SetValueWithoutNotify");
        }

        public override void OnInspectorGUI() {
            serializedObject.Update();

            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(_isOnProp);
            if (EditorGUI.EndChangeCheck())
                _setValueWithoutNotifyMI?.Invoke(serializedObject.targetObject, new object[]{ _isOnProp.boolValue });

            serializedObject.ApplyModifiedProperties();

            EditorGUILayout.Space();

            base.OnInspectorGUI();
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}