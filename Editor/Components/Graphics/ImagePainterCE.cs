﻿using UIBeautify.Runtime.Components.Graphics;
using UnityEditor;
using UnityEditor.UI;

namespace UIBeautify.Editors.Components.Graphics {
    [CanEditMultipleObjects]
    [CustomEditor(typeof(ImagePainter), true)]
    public class ImagePainterCE : ImageEditor {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Behaviour Callbacks
        protected override void OnEnable() {
            base.OnEnable();
        }

        public override void OnInspectorGUI() {
            base.OnInspectorGUI();

            serializedObject.Update();

            // --- Custom Fields ---
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Settings", EditorStyles.boldLabel);

            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_normalColor"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_selectedColor"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_highlightColor"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_selectedHighlightColor"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_pressedColor"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_disabledColor"));
            EditorGUILayout.Space();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_duration"));

            serializedObject.ApplyModifiedProperties();
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}