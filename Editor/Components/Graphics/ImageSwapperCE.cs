﻿using UIBeautify.Runtime.Components.Graphics;
using UnityEditor;
using UnityEditor.UI;

namespace UIBeautify.Editors.Components.Graphics {
    [CanEditMultipleObjects]
    [CustomEditor(typeof(ImageSwapper), true)]
    public class ImageSwapperCE : ImageEditor {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Behaviour Callbacks
        protected override void OnEnable() {
            base.OnEnable();
        }

        public override void OnInspectorGUI() {
            base.OnInspectorGUI();

            serializedObject.Update();

            // --- Custom Fields ---
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Settings", EditorStyles.boldLabel);

            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_defaultSprite"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_spriteToSwap"));

            serializedObject.ApplyModifiedProperties();
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}