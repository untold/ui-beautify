﻿using UIBeautify.Runtime.Components.Graphics;
using UnityEditor;
using UnityEditor.UI;

namespace UIBeautify.Editors.Components.Graphics {
    [CanEditMultipleObjects]
    [CustomEditor(typeof(ImageRounded), true)]
    public class ImageRoundedCE : ImageEditor {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Overrides
        public override void OnInspectorGUI() {
            base.OnInspectorGUI();

            serializedObject.Update();

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Settings", EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_radius"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_resolution"));
            var fillProp = serializedObject.FindProperty("m_fill");
            EditorGUILayout.PropertyField(fillProp);
            if (!fillProp.boolValue)
                EditorGUILayout.PropertyField(serializedObject.FindProperty("m_radiusInner"));

            serializedObject.ApplyModifiedProperties();
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}