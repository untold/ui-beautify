﻿using System.Reflection;
using UIBeautify.Runtime.Components.Graphics;
using UnityEditor;
using UnityEditor.UI;
using UnityEngine;

namespace UIBeautify.Editors.Components.Graphics {
    [CanEditMultipleObjects]
    [CustomEditor(typeof(ImageZoomer), true)]
    public class ImageZoomerCE : ImageEditor {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Behaviour Callbacks
        public override void OnInspectorGUI() {
            serializedObject.Update();

            // --- Image Fields ---
            EditorGUILayout.LabelField("Image", EditorStyles.boldLabel);

            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_Sprite"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_Color"));

            EditorGUILayout.BeginHorizontal();
            var raycastTargetProp = serializedObject.FindProperty("m_RaycastTarget");
            var maskableProp = serializedObject.FindProperty("m_Maskable");
            var preserveAspectProp = serializedObject.FindProperty("m_PreserveAspect");
            raycastTargetProp.boolValue = GUILayout.Toggle(raycastTargetProp.boolValue, "Raycast Target", EditorStyles.miniButtonLeft);
            maskableProp.boolValue = GUILayout.Toggle(maskableProp.boolValue, "Maskable", EditorStyles.miniButtonMid);
            preserveAspectProp.boolValue = GUILayout.Toggle(preserveAspectProp.boolValue, "Preserve Aspect", EditorStyles.miniButtonRight);
            EditorGUILayout.EndHorizontal();

            if (GUILayout.Button("Set Native Size")) {
                Undo.RegisterCompleteObjectUndo(serializedObject.targetObject, "Set Native Size");
                typeof(ImageZoomer).InvokeMember(nameof(ImageZoomer.SetNativeSize), BindingFlags.InvokeMethod, null, serializedObject.targetObject, null);
            }

            // --- Custom Fields ---
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Settings", EditorStyles.boldLabel);

            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_normalZoom"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_hoverZoom"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_time"));

            serializedObject.ApplyModifiedProperties();
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}