﻿using System;
using UIBeautify.Runtime.Core;
using UIBeautify.Runtime.Utility;
using UnityEditor;
using UnityEngine;

namespace UIBeautify.Editors.Core {
    [CustomPropertyDrawer(typeof(BNavigation))]
    public class BNavigationPCE : PropertyDrawer {
        #region Public Variables
        #endregion

        #region Private Variables
        private SerializedProperty _modeProp;
        private SerializedProperty _wrapAroundProp;
        private SerializedProperty[] _selectProp = new SerializedProperty[4];
        #endregion

        #region Properties
        #endregion

        #region Behaviour Callbacks
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
            _modeProp ??= property.FindPropertyRelative("<Mode>k__BackingField");

            var paddingVertical = 8f + EditorGUIUtility.singleLineHeight;

            if (!property.isExpanded)
                return paddingVertical + 4f;

            return paddingVertical + _modeProp.enumValueFlag switch {
                0 or 3 => EditorGUIUtility.singleLineHeight,
                1 or 2 => EditorGUIUtility.singleLineHeight * 2 + EditorGUIUtility.standardVerticalSpacing,
                4 => EditorGUIUtility.singleLineHeight * 5 + EditorGUIUtility.standardVerticalSpacing * 4,
                _ => 0f
            };
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            _modeProp ??= property.FindPropertyRelative("<Mode>k__BackingField");
            _wrapAroundProp ??= property.FindPropertyRelative("<WrapAround>k__BackingField");
            _selectProp[0] ??= property.FindPropertyRelative("<SelectOnUp>k__BackingField");
            _selectProp[1] ??= property.FindPropertyRelative("<SelectOnDown>k__BackingField");
            _selectProp[2] ??= property.FindPropertyRelative("<SelectOnLeft>k__BackingField");
            _selectProp[3] ??= property.FindPropertyRelative("<SelectOnRight>k__BackingField");

            var headerRect = new Rect(position) {
                height = EditorGUIUtility.singleLineHeight + 2
            };
            GUI.Box(headerRect, GUIContent.none, "RL Header");
            headerRect.x += 16;
            property.isExpanded = EditorGUI.Foldout(headerRect, property.isExpanded, label, true);

            var contentsRect = new Rect(position) {
                y = headerRect.yMax,
                height = position.height - headerRect.height
            };
            GUI.Box(contentsRect, GUIContent.none, "RL Background");

            if (!property.isExpanded)
                return;

            position.x += 4;
            position.y += 4 + EditorGUIUtility.singleLineHeight;
            position.width -= 8;
            position.height -= 8 - EditorGUIUtility.singleLineHeight;

            var modeRect = new Rect(position) {
                height = EditorGUIUtility.singleLineHeight
            };

            EditorGUI.BeginProperty(position, label, property);

            // EditorGUI.PropertyField(modeRect, _modeProp);
            _modeProp.enumValueIndex = EditorGUI.Popup(modeRect, _modeProp.displayName, _modeProp.enumValueIndex, Enum.GetNames(typeof(NavigationMode)));

            switch (_modeProp.enumValueFlag) {
                case 0: // None
                    break;
                case 1 or 2: // Hor or Ver
                    var wrapAroundRect = new Rect(modeRect) { y = modeRect.yMax + EditorGUIUtility.standardVerticalSpacing };
                    EditorGUI.PropertyField(wrapAroundRect, _wrapAroundProp);
                    break;
                case 3: // Auto
                    break;
                case 4: // Explicit
                    var selectDirRect = new Rect(modeRect) { y = modeRect.yMax + EditorGUIUtility.standardVerticalSpacing };
                    foreach (var dirProp in _selectProp) {
                        EditorGUI.PropertyField(selectDirRect, dirProp);
                        selectDirRect.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
                    }
                    break;
            }

            EditorGUI.EndProperty();
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion
    }
}